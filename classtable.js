var LEFT_WIDTH = 35;
var HEAD_HEIGHT = 40;
var IOS_HEIGHT = 71;
var cell_width;
var cell_height;
var course_data = "";
var course_matrix;
var teacher_array;

//TODO 根据主题色修改Course类DOM的底色
//可以在这里写一个setColor函数，在TMRightCourseViewController.m中的webViewDidFinishLoad中调用
//[self.webView stringByEvaluatingJavaScriptFromString:@"setColor(para)"];

function InitStyle(){
  cell_height = ((window.screen.height - HEAD_HEIGHT - IOS_HEIGHT)/7).toString() + "px";
  cell_width = ((window.screen.width - LEFT_WIDTH)/5 - 1).toString() + "px";
 //Let's do this
    
  var mondays = document.getElementsByClassName("Monday");
  for(var i=0; i<mondays.length; i++){
    mondays[i].style.width = cell_width;
  }
  
  var tuesdays = document.getElementsByClassName("Tuesday");
  for(var i=0; i<tuesdays.length; i++){
    tuesdays[i].style.width = cell_width;
  }
  
  var Wednesdays = document.getElementsByClassName("Wednesday");
  for(var i=0; i<Wednesdays.length; i++){
    Wednesdays[i].style.width = cell_width;
  }
  
  var Thursdays = document.getElementsByClassName("Thursday");
  for(var i=0; i<Thursdays.length; i++){
    Thursdays[i].style.width = cell_width;
  }
  
  var Fridays = document.getElementsByClassName("Friday");
  for(var i=0; i<Fridays.length; i++){
    Fridays[i].style.width = cell_width;
  }
  
  var Saturdays = document.getElementsByClassName("Saturday");
  for(var i=0; i<Saturdays.length; i++){
    Saturdays[i].style.width = cell_width;
  }
  
  var Sundays = document.getElementsByClassName("Sunday");
  for(var i=0; i<Sundays.length; i++){
    Sundays[i].style.width = cell_width;
  }
  
  var lines = document.getElementsByClassName("Line");
  for(var i=0; i<lines.length; i++){
    lines[i].style.height = cell_height;
  }
  
  var classnums = document.getElementsByClassName("Class_Num");
  for(var i=0; i<classnums.length; i++){
    classnums[i].style.paddingTop = (((window.screen.height - HEAD_HEIGHT)/7 - 13)/2).toString() + "px";
  }
  sendLoadRequest();
}

function fillinClasses(){
    //这里证明后端没有拿到数据
    if(course_data == "")
        return false;

    teacher_array = new Array();
    for(var i=0;i<course_data.users.length;i++){
        var teacher = new Object();
        teacher.name = course_data.users[i].name;
        teacher.id = course_data.users[i].id;
        teacher.photo = course_data.users[i].photo;
        //teacher.description has nothing using
    }
    
    //课程矩阵初始化
    course_matrix = new Array();
    for(var i=0;i<7;i++){
        course_matrix[i] = new Array();
        for(var j=0;j<7;j++){
            course_matrix[i][j] = new Array();
        }
    }
    
    //将原始数据填充到课程矩阵
    for(var i=0;i<course_data.classes.length;i++){
        //TODO 识别当前week,判断是否落在start_week，end_week区间内
        for(var j=0;j<course_data.classes[i].schedule.schedule_weekly.length;j++){
            var day = course_data.classes[i].schedule.schedule_weekly[j].day;
            //大客节小课节的转换
            var startSection = Math.ceil((course_data.classes[i].schedule.schedule_weekly[j].start_section)/2);
            var endSection = Math.floor((course_data.classes[i].schedule.schedule_weekly[j].end_section)/2);
            //将每节课填充到课程矩阵
            for(var k=startSection-1; k < endSection; k++){
                //每节课必要的信息
                var classToAdd = new Object();
                classToAdd.id = course_data.classes[i].id;
                classToAdd.name = course_data.classes[i].course_name;
                classToAdd.teachers_list = course_data.classes[i].teachers_list;
                classToAdd.building = course_data.classes[i].schedule.schedule_weekly[j].building;
                classToAdd.room = course_data.classes[i].schedule.schedule_weekly[j].room;
                //去重
                var duplicatedFlag = false;
                for(var t=0;t<course_matrix[day-1][k].length;t++){
                    if(course_matrix[day-1][k][t].id == classToAdd.id){
                        duplicatedFlag = true;
                    }
                }
                //向课程矩阵添加一堂课
                if(!duplicatedFlag){
                    var count = course_matrix[day-1][k].length;
                    course_matrix[day-1][k][count] = classToAdd;
                }
            }
        }
    }
    
    //将课程矩阵的信息填充到界面
    for(var i=0;i<7;i++){
        for(var j=0;j<7;j++){
            if(course_matrix[i][j].length == 0){
                continue;
            }
            if(course_matrix[i][j].length == 1){
                var DOMid = (i+1).toString() +"-" + (j+1).toString();
                var DOMlink = document.createElement("a");
                DOMlink.setAttribute("onclick","showDetail("+(i+1).toString()+","+(j+1).toString()+")");
                DOMlink.innerHTML = '<div class="Course"><div class="Course_Name"><p>'+ course_matrix[i][j][0].name+'</p></div><div class="Course_Place"><p>'+course_matrix[i][j][0].building+course_matrix[i][j][0].room+'</p></div></div>';
                document.getElementById(DOMid).appendChild(DOMlink);
            }
            else{
                var DOMid = (i+1).toString() +"-" + (j+1).toString();
                var DOMlink = document.createElement("a");
                DOMlink.setAttribute("onclick","showDetail("+(i+1).toString()+","+(j+1).toString()+")");
                DOMlink.innerHTML = '<div class="Course"><div class="Course_Name"><p>'+ course_matrix[i][j].length+'门课程</p></div><div class="Course_Place"><p>点击查看详情</p></div></div>';
                document.getElementById(DOMid).appendChild(DOMlink);
            }
        }
    }
}

//TODO 课程详情的设计出来之后，将课程信息填充到DOM
function showDetail(day,time){
    var info = course_matrix[day-1][time-1];
    var alertInfo = "";
    for(var i=0;i<info.length;i++){
        alertInfo = alertInfo + "===第" + (i+1).toString() + "个课程===\n名称" + info[i].name + "\n位置："+ info[i].building + info[i].room +"\n";
    }
    alert(alertInfo);
}

function sendLoadRequest(){
    //采用仿Cordova的机智实现原生和HTML的通讯
    var iFrame;
    iFrame = document.createElement("iFrame");
    iFrame.setAttribute("src","gap://load");
    iFrame.setAttribute("style","display:none");
    iFrame.setAttribute("height","0px");
    iFrame.setAttribute("width","0px");
    iFrame.setAttribute("frameborder","0");
    document.body.appendChild(iFrame);
    //Async shouleStartLoadWithRequest
    iFrame.parentNode.removeChild(iFrame);
    iFrame = null;
}
